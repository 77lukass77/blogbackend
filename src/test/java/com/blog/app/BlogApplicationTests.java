package com.blog.app;

import com.blog.app.BlogApplication;
import com.blog.app.model.Blog;
import com.blog.app.model.User;
import com.blog.app.repository.BlogRepository;
import com.blog.app.repository.UserRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = BlogApplication.class)
public class BlogApplicationTests {

    public static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    @Autowired
    private BlogRepository blogRepository;

    @Autowired
    private UserRepository userRepository;

    private RestTemplate restTemplate = new TestRestTemplate();

    @Test
    public void testCreateUser() throws JsonProcessingException,IOException {

        Map<String,Object> requestBody = new HashMap<>();
        requestBody.put("username", "lukasz");
        requestBody.put("email", "luk@luk.com");
        requestBody.put("login","luk");
        requestBody.put("password","test123");

        HttpHeaders reqestHeaders = new HttpHeaders();
        reqestHeaders.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<String> httpEntity =
            new HttpEntity<>(OBJECT_MAPPER.writeValueAsString(requestBody), reqestHeaders);

        Map<String, Object> apiResponse =
            restTemplate.postForObject("http://localhost:8080/api/user",
                httpEntity, Map.class, Collections.EMPTY_MAP);

        assertNotNull(apiResponse);

        User userdb = userRepository.findByUsername("lukasz");
        assertEquals("lukasz", userdb.getUsername());
        assertEquals("luk", userdb.getLogin());
        assertEquals("luk@luk.com", userdb.getEmail());


    }

}
