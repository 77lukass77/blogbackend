package com.blog.app.repository;

import com.blog.app.model.Blog;
import com.sun.org.apache.xpath.internal.operations.String;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Lukasz on 2016-11-17.
 */
public interface BlogRepository extends MongoRepository<Blog, String> {

    public Blog findOne(String id);

}
