package com.blog.app.model;

import org.springframework.data.annotation.Id;

/**
 * Created by Lukasz on 2016-11-17.
 */
public class Blog {


    @Id
    public String id;

    public String title;
    public String txt;
    public String author;
    public String date;
    public String tags;
    public int counter;
    public String author_ip;
    public String short_desc;


    public Blog() {

    }

    public Blog(String title, String txt, String author, String date) {
        this.title = title;
        this.txt = txt;
        this.author = author;
        this.date = date;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setTxt(String txt) {
        this.txt = txt;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getId() {

        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getTxt() {
        return txt;
    }

    public String getAuthor() {
        return author;
    }

    public String getDate() {
        return date;
    }
}

