package com.blog.app.controller;

import com.blog.app.model.Blog;
import com.blog.app.model.Logs;
import com.blog.app.model.User;
import com.blog.app.repository.BlogRepository;
import com.blog.app.repository.LogsRepository;
import com.blog.app.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class Controller {

    private BlogRepository blogRepository;
    private LogsRepository logsRepository;
    private UserRepository userRepository;

    @Autowired
    public Controller(BlogRepository blogRepository, LogsRepository logsRepository, UserRepository userRepository) {
        this.blogRepository = blogRepository;
        this.logsRepository = logsRepository;
        this.userRepository = userRepository;
    }

    @CrossOrigin(origins = "http://localhost:9000")
    @RequestMapping(value = "/api/blog", method = RequestMethod.GET)
    public List<Blog> funckja() {

        return blogRepository.findAll();

    }

    @CrossOrigin(origins = "http://localhost:9000")
    @RequestMapping(value = "/api/blog/showlast", method = RequestMethod.GET)
    public Page<Blog> showlast() {

        return blogRepository.findAll(new PageRequest(0,5));

    }

    @CrossOrigin(origins = "http://localhost:9000")
    @RequestMapping(value = "/api/blog/{blogId}", method = RequestMethod.GET)
    public @ResponseBody Blog selectById(@PathVariable String blogId){
        //Blog blog=  blogRepository.findOne(blogId);
        return null;
    }

    @CrossOrigin(origins = "http://localhost:9000")
    @RequestMapping(value = "/api/blog", method = RequestMethod.POST)
    public Blog add(@RequestBody Blog blog){
        return blogRepository.save(blog);
    }

    @CrossOrigin(origins = "http://localhost:9000")
    @RequestMapping(value = "/api/logs", method = RequestMethod.POST)
    public Logs add(@RequestBody Logs logs) {
        return logsRepository.save(logs);
    }

    /* NOWE */

    @CrossOrigin(origins = "http://localhost:9000")
    @RequestMapping(value = "/api/user", method = RequestMethod.POST)
    public @ResponseBody User add(@RequestBody User user){
        return userRepository.save(user);
    }

  @CrossOrigin(origins = "http://localhost:9000")
  @RequestMapping(value = "/api/userxml", method = RequestMethod.POST, produces = MediaType.APPLICATION_XML_VALUE)
  public @ResponseBody User addXml(@RequestBody User user){
    return userRepository.save(user);
  }


    //zwracam xmla
    @CrossOrigin(origins = "http://localhost:9000")
    @RequestMapping(value = "/api/userlist", method = RequestMethod.GET, produces = MediaType.APPLICATION_XML_VALUE)
    public @ResponseBody List<User> userListXML(){
        return userRepository.findAll();
    }

    @CrossOrigin(origins = "http://localhost:9000")
    @RequestMapping(value = "/api/userlistjson", method = RequestMethod.GET)
    public @ResponseBody List<User> userListJSON(){
        return userRepository.findAll();
    }

    /* koniec nowych */
}
