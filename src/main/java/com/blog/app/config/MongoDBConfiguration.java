package com.blog.app.config;

import com.mongodb.Mongo;
import com.mongodb.MongoClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

/**
 * Created by Lukasz on 2016-11-17.
 */
@Configuration
@EnableMongoRepositories("com.blog.app.repository")
public class MongoDBConfiguration extends AbstractMongoConfiguration {

    @Value("${spring.data.mongodb.host}")
    private String host;

    @Value("${spring.data.mongodb.port}")
    private Integer port;

    @Value("${spring.data.mongodb.database}")
    private String dbname;

    @Override
    protected String getDatabaseName() {
        return dbname;
    }

    @Override
    public Mongo mongo() throws Exception {
        return new MongoClient(host+":"+port);
    }
}
